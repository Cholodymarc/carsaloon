package carcreator.controller;

import model.car.Car;
import model.car.Colour;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import scanner.view.ScannerMVC;

public class CarCreatorControllerTest {
    Car car = new Car();
    double currentWaller = 200000;
    ScannerMVC.Controller scannerMock = Mockito.mock(ScannerMVC.Controller.class);
    CarCreatorController tested = new CarCreatorController(car, scannerMock, currentWaller);

    @Test
    public void test(){
        Mockito.when(scannerMock.pickOption()).thenReturn(2);
        tested.pickColor();
        Assert.assertEquals(car.getColour(), Colour.BLUE);
    }
}