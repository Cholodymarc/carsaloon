package scanner.view;

public interface ScannerMVC {

    interface Controller {

        int nextInt();
        int pickOption();
        double nextDouble();
        String nextLine();
    }

    interface View {

    }
}
