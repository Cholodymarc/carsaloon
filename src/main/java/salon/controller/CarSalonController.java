package salon.controller;

import carcreator.controller.CarCreatorController;
import carcreator.view.CarCreatorMVC;
import console.controller.ConsoleController;
import console.view.ConsoleMVC;
import menu.controller.MainMenuController;
import menu.view.MainMenu;
import menu.view.MainMenuMVC;
import model.car.Car;
import model.user.IPerson;
import model.user.Person;
import salon.view.CarSalonMVC;
import scanner.controller.ScannerController;
import scanner.view.ScannerMVC;

public class CarSalonController implements CarSalonMVC.Controller {

    private MainMenuMVC.View mainMenuView;
    private CarSalonMVC.View carSalonView;

    private CarSalonMVC.Controller controller;
    private ScannerMVC.Controller scannerController;
    private MainMenuMVC.Controller mainMenuController;
    private CarCreatorMVC.Controller carCreatorController;
    private ConsoleMVC.Controller consoleController;

    private IPerson person;
    private Car car;

    public CarSalonController() {
        this.scannerController = new ScannerController();
        this.consoleController = new ConsoleController();
        this.car = new Car();
        this.person = new Person(scannerController.nextLine(), scannerController.nextDouble());
        this.carCreatorController = new CarCreatorController(car, scannerController, person.getWallet().getCashAmount());
        this.mainMenuController = new MainMenuController();
//        this.mainMenuView = new MainMenu();
    }

    @Override
    public void run() {

        printMenu(new MainMenu());

    }

    @Override
    public void printMenu(MainMenuMVC.View mainMenuView) {
        mainMenuView.showAvailableOptions();
//        showAvailableCars();
//        this.mainMenuController.goToColorPick();
    }

    @Override
    public void attach(MainMenuMVC.View view) {

    }

    //    @Override
//    public void attach(CarSalonMVC.View carSalonView) {
//        carSalonView.showAvailableCars();
//    }
}
