package salon.view;

import menu.view.MainMenuMVC;

public interface CarSalonMVC {

    interface Controller{
        void run();
        void printMenu(MainMenuMVC.View mainMenuView);
        void attach(MainMenuMVC.View view);
    }

    interface View {
        void welcomeMessaage();
        void showAvailableCars();


    }
}
