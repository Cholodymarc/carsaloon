package salon.view;

import console.controller.ConsoleController;
import console.view.ConsoleMVC;
import menu.view.MainMenu;
import menu.view.MainMenuMVC;
import model.car.AvailableCars;

public class CarSalon implements CarSalonMVC.View {
    private ConsoleMVC.Controller consoleController;

    private MainMenuMVC.View mainMenuView;

    public CarSalon() {
        this.consoleController = new ConsoleController();
        this.mainMenuView = new MainMenu();
    }

    public void welcomeMessaage(){
        System.out.println("Hello, please enter your name and cash amount you have: ");
    }

    public void showAvailableCars(){
        for(Enum value: AvailableCars.values()){
            consoleController.printLine(value.toString());
        }
    }



}
