package model.car;

public enum AvailableCars {
    DEFAULT("DefaultName", 0), AUDI("Audi", 80000), BMW("BMW", 120000), FIAT("Fiat", 60000);
    private String name;
    private double price;

    AvailableCars(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public AvailableCars pickBasicCar(int number) {
        switch (number) {
            case 0:
                return AUDI;
            case 1:
                return BMW;
            case 2:
                return FIAT;
            default:
                return DEFAULT;
        }
    }
}
