package model.car;

public enum BodyCar {
    NONE(0),
    SEDAN(0),
    KOMBI(1000),
    HATCHBACK(1000),
    PICKUP(2000);

    private double price;

    BodyCar(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}
