package model.car;

public enum Colour {
    NONE(0),
    RED(0),
    BLUE(0),
    GREEN(500),
    BLACK(600);

    private double price;

    Colour(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}