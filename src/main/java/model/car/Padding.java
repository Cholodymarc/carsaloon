package model.car;

public enum Padding {
    NONE(0),
    VELOUR(0),
    LEATHER(1000),
    ECOSKIN(2000);

    private double price;

    Padding(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}
