package model.car;

public enum FuelType {
    NONE(0),
    GAS(0),
    OIL(10000),
    ELECTRIC(15000);

    private double price;

    FuelType(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}
