package model.user;

public class Wallet {
    private double cashAmount;

    public Wallet(double cashAmount) {
        this.cashAmount = cashAmount;
    }

    public double getCashAmount() {
        return cashAmount;
    }

    public void increase(double amount) {
        this.cashAmount += amount;
    }

    public void decrease(double amount) {
        this.cashAmount += amount;
    }
}
