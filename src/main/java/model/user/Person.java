package model.user;

public class Person implements IPerson{
    private String name;
    private Wallet wallet;

    public Person(String name, double walletCashAmount) {
        this.name = name;
        this.wallet = new Wallet(walletCashAmount);
    }

    public String getName() {
        return name;
    }

    public Wallet getWallet() {
        return wallet;
    }
}
