package model.user;

public interface IPerson {
    String getName();

   Wallet getWallet();

}
