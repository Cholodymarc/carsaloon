package menu.view;

public interface MainMenuMVC {
    interface Controller {
        void attach(View view);

        void goToColorPick();
        void goToFuelTypePick(int amountToSubtract);
        void goToPaddingPick();
        void goToBodyCarPick();
    }

    interface View {
        void showAvailableOptions();
    }
}
