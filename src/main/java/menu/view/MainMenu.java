package menu.view;

import console.controller.ConsoleController;
import console.view.ConsoleMVC;
import model.menu.MenuOptions;

public class MainMenu implements MainMenuMVC.View {
    private ConsoleMVC.Controller consoleController;

    public MainMenu() {
        this.consoleController = new ConsoleController();
    }

    @Override
    public void showAvailableOptions() {
        for(MenuOptions option : MenuOptions.values()){
            consoleController.printLine(option.toString());
        }
    }


}
