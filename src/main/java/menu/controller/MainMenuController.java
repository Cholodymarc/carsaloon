package menu.controller;
import menu.view.MainMenuMVC;

public class MainMenuController implements menu.view.MainMenuMVC.Controller {
    @Override
    public void attach(MainMenuMVC.View view) {

    }

    @Override
    public void goToColorPick() {

    }


    @Override
    public void goToFuelTypePick(int amountToSubtract) {

    }

    @Override
    public void goToPaddingPick() {

    }

    @Override
    public void goToBodyCarPick() {

    }
}
