package carcreator.view;

import model.car.Car;

public interface CarCreatorMVC {

    interface View {

    }

    interface Controller {
        void pickBasicCar();
        void pickColor();
        void pickFuelType();
        void pickBodyCar();
        void pickPadding();
        Car getCar();
    }
}
