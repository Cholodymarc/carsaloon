package carcreator.controller;

import carcreator.view.CarCreatorMVC;
import model.car.*;
import scanner.view.ScannerMVC;

public class CarCreatorController implements CarCreatorMVC.Controller {

    private Car car;
    private ScannerMVC.Controller scannerController;
    private double currentWallet;

    public CarCreatorController(Car car, ScannerMVC.Controller scannerController, double currentWallet) {
        this.scannerController = scannerController;
        this.currentWallet = currentWallet;
        this.car = car;
    }

    @Override
    public void pickBasicCar() {
        int option = scannerController.pickOption();
        this.car.setName(AvailableCars.DEFAULT.pickBasicCar(option).getName());
        this.car.setBasicPrice(AvailableCars.DEFAULT.pickBasicCar(option).getPrice());
    }

    @Override
    public void pickColor() {
        int option = scannerController.pickOption();

        Colour pickedColor = Colour.NONE;
        switch (option) {
            case 1:
                pickedColor = Colour.RED;
                break;
            case 2:
                pickedColor = Colour.BLUE;
                break;
            case 3:
                pickedColor = Colour.GREEN;
                break;
            case 4:
                pickedColor = Colour.BLACK;
                break;
            default:
                pickColor();
                break;
        }
        car.setColour(pickedColor);
    }

    @Override
    public void pickFuelType() {
        int option = scannerController.pickOption();

        FuelType pickedFuelType = FuelType.NONE;
        switch (option) {
            case 1:
                pickedFuelType =FuelType.GAS;
                break;
            case 2:
                pickedFuelType =FuelType.OIL;
                break;
            case 3:
                pickedFuelType =FuelType.ELECTRIC;
                break;
            case 0:
                pickColor();
                break;
            default:
                pickFuelType();
                break;
        }
        car.setFuelType(pickedFuelType);
    }

    @Override
    public void pickBodyCar() {
        int option = scannerController.pickOption();

        BodyCar pickedBodyCar = BodyCar.NONE;
        switch (option) {
            case 1:
                pickedBodyCar = BodyCar.HATCHBACK;
                break;
            case 2:
                pickedBodyCar = BodyCar.SEDAN;
                break;
            case 3:
                pickedBodyCar = BodyCar.KOMBI;
                break;
            case 4:
                pickedBodyCar = BodyCar.PICKUP;
            case 0:
                pickFuelType();
                break;
            default:
                pickBodyCar();
                break;
        }
        car.setBodyCar(pickedBodyCar);
        pickPadding();
    }

    @Override
    public void pickPadding() {
        int option = scannerController.pickOption();

        Padding pickedPadding = Padding.NONE;
        switch (option) {
            case 1:
                pickedPadding = Padding.VELOUR;
                break;
            case 2:
                pickedPadding = Padding.LEATHER;
                break;
            case 3:
                pickedPadding = Padding.ECOSKIN;
                break;
            case 0:
                pickBodyCar();
                break;
            default:
                pickPadding();
                break;
        }
        car.setPadding(pickedPadding);
    }

    @Override
    public Car getCar() {
        return this.car;
    }
}
