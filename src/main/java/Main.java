import salon.controller.CarSalonController;
import salon.view.CarSalonMVC;

public class Main {
    public static void main(String[] args) {
        CarSalonMVC.Controller carSalon = new CarSalonController();
        carSalon.run();
    }
}
