package console.controller;

import console.view.ConsoleMVC;

public class ConsoleController implements ConsoleMVC.Controller {
    @Override
    public void printLine(String line) {
        System.out.println(line);
    }
}
