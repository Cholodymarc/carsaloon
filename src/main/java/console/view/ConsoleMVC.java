package console.view;

public interface ConsoleMVC {
    interface Controller {
        void printLine(String line);
    }
}
